# README #

Сервер для android-приложения, которое позволяет вести заметки. 
Andoird приложение можно найти тут https://bitbucket.org/loginovas/note-to-forget-android.

### Как запустить у себя ###
Можно запустить разными способами.

#### Используя билд
* Убедиться, что java установлена в системе
* Скачать билд https://bitbucket.org/loginovas/note-to-forget-server/downloads/note-to-forget-server.zip
* Распаковать его где нибудь
* Открыть терминал, перейти в bin директорию
* Для запуска сервера на порту 8085 (по умолчанию) выполните команду
  ```./note-to-forget-server``` (Linux/MacOS) или ```note-to-forget-server.bat``` (Windows)
* Для переопределения порта используйте опцию ```-port=8080```. 
  **Имейте в виду**, что android приложение использут 8085 порт

#### Используя IDEA
* Открыть проект, дождаться окончания инициализации
* Открыть файл ```iloginovas.notetoforget.server.Main```
* Запустить ```main()```

#### Используя Gradle
* Открыть терминал
* Перейти в директорию проекта сервера (note-to-forget-server)
* Выполнить ```gradle run```


### Как изменить порт в коде ###

Сменить порт можно в конфигурационном файле src/main/resources/application.conf.
**Внимание:** при смене порта сервера не забудьте поменять порт в android-приложении. Информация об этом
есть в readme.md репозитория https://bitbucket.org/loginovas/note-to-forget-android