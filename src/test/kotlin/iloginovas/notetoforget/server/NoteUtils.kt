package iloginovas.notetoforget.server

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.common.NoteId
import iloginovas.notetoforget.server.model.Note
import java.util.*

object NoteUtils {

    fun randomApiNote(): Api.Note = randomNote().toApi()

    fun randomNote(syncVersion: Long = 0): Note {
        val noteId = NoteId.generate(Date())
        return Note(
            id = noteId, title = randomTitle(), text = randomText(),
            lastUpdate = NoteId.getSecondsSinceEpoch(noteId),
            deleted = false, syncVersion = syncVersion
        )
    }

    fun updateApiNote(note: Api.Note): Api.Note {
        val lastUpdate = Date().time / 1000
        return note.copy(title = randomTitle(), text = randomText(), lastUpdate = lastUpdate)
    }

    fun updateNote(note: Note): Note {
        val lastUpdate = Date().time / 1000
        return note.copy(
            title = randomTitle(), text = randomText(), lastUpdate = lastUpdate
        )
    }

    fun randomTitle() = "Title text : ${UUID.randomUUID()}"
    fun randomText() = "Note text : ${UUID.randomUUID()}"
}

private fun Note.toApi() = Api.Note(id, title, text, lastUpdate)