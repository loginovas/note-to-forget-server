@file:Suppress("MemberVisibilityCanBePrivate")

package iloginovas.notetoforget.server.service

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.server.NoteUtils
import iloginovas.notetoforget.server.TestDatabaseHelper
import iloginovas.notetoforget.server.data.NoteStorage
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.TransactionSupport
import iloginovas.notetoforget.server.data.impl.NoteStorageImpl
import iloginovas.notetoforget.server.data.impl.db.NoteTable
import iloginovas.notetoforget.server.data.impl.db.UserNoteStorageVersionTable
import iloginovas.notetoforget.server.model.UserSession
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class NoteServiceTest : TransactionSupport {

    companion object {
        val userSession = UserSession("sessionId", 1)

        val dbHelper = TestDatabaseHelper(
            schema = arrayOf(
                NoteTable(userSession.userId), UserNoteStorageVersionTable
            )
        ).also { it.init(createSchema = false) }

        @AfterClass
        fun afterAll() {
            dbHelper.destroy()
        }
    }

    override val transactionManager: TransactionManager get() = dbHelper.transactionManager
    val storage: NoteStorage = NoteStorageImpl(userSession.userId)
    val noteService = NoteService(transactionManager, noteStorageProvider = { storage })

    @Before
    fun beforeTest() {
        dbHelper.createSchema()
    }

    @After
    fun afterTest() {
        dbHelper.dropSchema()
    }

    @Test
    fun test1(): Unit = runBlocking {
        kotlin.run {
            val syncRequest = Api.NoteChanges(-1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(0, emptyList(), emptyList())
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        val note1: Api.Note = NoteUtils.randomApiNote()
        kotlin.run {
            val syncRequest = Api.NoteChanges(
                0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1)
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(1, emptyList(), emptyList())
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(0, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                1, notesToBeRemoved = emptyList(),
                notesToBePut = listOf(note1)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        val note2: Api.Note = NoteUtils.randomApiNote()
        kotlin.run {
            val syncRequest = Api.NoteChanges(
                1, notesToBeRemoved = emptyList(), notesToBePut = listOf(note2)
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                2, notesToBeRemoved = emptyList(), notesToBePut = emptyList()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }

    @Test
    fun test2(): Unit = runBlocking {
        val note1: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 1
            userSession,
            Api.NoteChanges(0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1))
        )

        val note2: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 2
            userSession,
            Api.NoteChanges(1, notesToBeRemoved = emptyList(), notesToBePut = listOf(note2))
        )

        val note3: Api.Note = NoteUtils.randomApiNote()
        kotlin.run {
            val syncRequest = Api.NoteChanges(
                -1, notesToBeRemoved = emptyList(), notesToBePut = listOf(note3)
            )
            val syncResponse = noteService.sync(userSession, syncRequest) // after sync the version is 3
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1, note2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(-1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, emptyList(), notesToBePut = listOf(note1, note2, note3)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }

    @Test
    fun test3(): Unit = runBlocking {
        val note1: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 1
            userSession,
            Api.NoteChanges(0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1))
        )

        val note2: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 2
            userSession,
            Api.NoteChanges(1, notesToBeRemoved = emptyList(), notesToBePut = listOf(note2))
        )

        kotlin.run {
            val syncRequest = Api.NoteChanges(
                2, notesToBeRemoved = listOf(note1.id), emptyList()
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, emptyList(), notesToBePut = emptyList()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(3, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, emptyList(), emptyList()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(2, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1.id), emptyList()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1.id), notesToBePut = listOf(note2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(0, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1.id), notesToBePut = listOf(note2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }

    @Test
    fun test4(): Unit = runBlocking {
        val note1: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 1
            userSession,
            Api.NoteChanges(0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1))
        )

        val note1Updated: Api.Note = NoteUtils.updateApiNote(note1)
            .copy(lastUpdate = note1.lastUpdate + 200)

        kotlin.run {
            val syncRequest = Api.NoteChanges(
                1, emptyList(), notesToBePut = listOf(note1Updated)
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                2, notesToBeRemoved = listOf(), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(0, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                2, notesToBeRemoved = listOf(), notesToBePut = listOf(note1Updated)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                2, notesToBeRemoved = listOf(), notesToBePut = listOf(note1Updated)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }

    @Test
    fun test5(): Unit = runBlocking {
        val note1: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 1
            userSession,
            Api.NoteChanges(0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1))
        )

        val note1Updated: Api.Note = NoteUtils.updateApiNote(note1)
            .copy(lastUpdate = note1.lastUpdate + 200)

        kotlin.run {
            val syncRequest = Api.NoteChanges(
                1, emptyList(), notesToBePut = listOf(note1Updated)
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                2, notesToBeRemoved = listOf(), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        val note1Updated2: Api.Note = NoteUtils.updateApiNote(note1)
            .copy(lastUpdate = note1.lastUpdate + 500)

        kotlin.run {
            val syncRequest = Api.NoteChanges(
                1, emptyList(), notesToBePut = listOf(note1Updated2)
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(0, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf(note1Updated2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf(note1Updated2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(2, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf(note1Updated2)
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }

    @Test
    fun test6(): Unit = runBlocking {
        val note1: Api.Note = NoteUtils.randomApiNote()
        noteService.sync( // after sync the version is 1
            userSession,
            Api.NoteChanges(0, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1))
        )

        val note1Updated: Api.Note = NoteUtils.updateApiNote(note1)
            .copy(lastUpdate = note1.lastUpdate + 200)

        noteService.sync( // after sync the version is 2
            userSession,
            Api.NoteChanges(1, notesToBeRemoved = emptyList(), notesToBePut = listOf(note1Updated))
        )

        kotlin.run {
            val syncRequest = Api.NoteChanges(
                2, notesToBeRemoved = listOf(note1Updated.id), notesToBePut = listOf()
            )
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(0, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1Updated.id), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(1, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1Updated.id), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(2, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(note1Updated.id), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }

        kotlin.run {
            val syncRequest = Api.NoteChanges(3, emptyList(), emptyList())
            val syncResponse = noteService.sync(userSession, syncRequest)
            val expectedSyncResponse = Api.NoteChanges(
                3, notesToBeRemoved = listOf(), notesToBePut = listOf()
            )
            assertSyncResponseEquals(syncResponse, expectedSyncResponse)
        }
    }
}

private fun assertSyncResponseEquals(actual: Api.NoteChanges, expected: Api.NoteChanges) {
    assertEquals(expected.version, actual.version)

    assertEquals(expected.notesToBePut.size, actual.notesToBePut.size)
    assert(expected.notesToBePut.containsAll(actual.notesToBePut))

    assertEquals(expected.notesToBeRemoved.size, actual.notesToBeRemoved.size)
    assert(expected.notesToBeRemoved.containsAll(actual.notesToBeRemoved))
}