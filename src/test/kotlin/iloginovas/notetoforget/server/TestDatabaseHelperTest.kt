package iloginovas.notetoforget.server

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

object TestTable : IntIdTable(name = "test_table") {
    val stringValue = text("string_value")
}

class TestEntity(id: EntityID<Int>) : IntEntity(id) {
    var stringValue: String by TestTable.stringValue

    companion object : IntEntityClass<TestEntity>(TestTable)
}

class TestDatabaseHelperTest {

    private lateinit var dbHelper: TestDatabaseHelper

    @Before
    fun beforeTest() {
        dbHelper = TestDatabaseHelper(
            schema = arrayOf(TestTable)
        )
    }

    @After
    fun afterTest() {
        dbHelper.destroy()
    }

    private fun insert(): TestEntity = transaction {
        TestEntity.new {
            stringValue = UUID.randomUUID().toString()
        }
    }

    @Test(expected = Throwable::class)
    fun `when helper is not initialized then any access to db causes exception`() {
        insert() // should throws the exception
    }

    @Test
    fun `when schema is recreated then previous data is deleted`() {
        dbHelper.init()

        insert()
        transaction { assertEquals(1L, TestEntity.all().count()) }

        dbHelper.recreateSchema()
        transaction { assertEquals(0L, TestEntity.all().count()) }
    }
}