package iloginovas.notetoforget.server

import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.impl.TransactionManagerImpl
import iloginovas.notetoforget.server.data.impl.db.AppDatabase
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.TransactionManager as ExposedTransactionManager

class TestDatabaseHelper(
    val schema: Array<Table> = AppDatabase.tables
) {
    val db: Database get() = _db!!
    val transactionManager: TransactionManager get() = _transactionManager!!

    private var _db: Database? = null
    private var _transactionManager: TransactionManager? = null

    fun init(createSchema: Boolean = true) {
        check(_db == null)

        _db = AppDatabase.create(inMemory = true, createSchema = false)
        _transactionManager = TransactionManagerImpl(db)

        if (createSchema)
            createSchema()
    }

    fun recreateSchema() {
        dropSchema()
        createSchema()
    }

    fun createSchema() {
        transaction(db) { SchemaUtils.create(*schema) }
    }

    fun dropSchema() {
        transaction(db) { SchemaUtils.drop(*schema) }
    }

    fun destroy() {
        if (_db == null)
            return

        ExposedTransactionManager.closeAndUnregister(_db!!)
        _db = null
        _transactionManager = null
    }
}