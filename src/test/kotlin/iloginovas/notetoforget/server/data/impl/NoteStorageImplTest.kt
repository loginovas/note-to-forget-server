@file:Suppress("MemberVisibilityCanBePrivate")

package iloginovas.notetoforget.server.data.impl

import iloginovas.notetoforget.server.NoteUtils
import iloginovas.notetoforget.server.TestDatabaseHelper
import iloginovas.notetoforget.server.data.NoteStorage
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.TransactionSupport
import iloginovas.notetoforget.server.data.impl.db.NoteTable
import iloginovas.notetoforget.server.data.impl.db.UserNoteStorageVersionTable
import iloginovas.notetoforget.server.model.Note
import iloginovas.notetoforget.server.model.UserSession
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class NoteStorageImplTest : TransactionSupport {

    companion object {
        val userSession = UserSession("sessionId", 1)

        val dbHelper = TestDatabaseHelper(
            schema = arrayOf(
                NoteTable(userSession.userId), UserNoteStorageVersionTable
            )
        ).also { it.init(createSchema = false) }

        @AfterClass
        fun afterAll() {
            dbHelper.destroy()
        }
    }

    val storage: NoteStorage = NoteStorageImpl(userSession.userId)

    override val transactionManager: TransactionManager get() = dbHelper.transactionManager

    @Before
    fun beforeTest() {
        dbHelper.createSchema()
    }

    @After
    fun afterTest() {
        dbHelper.dropSchema()
    }

    /** Covers putNote(), getNotes(), markNoteAsDeleted() */
    @Test
    fun test1(): Unit = runBlocking {
        suspend fun assertThatStorageContains(vararg expectedNotes: Note) {
            val actual: Map<Long, Note> = readTransaction {
                storage.getNotes().associateBy { it.id }
            }

            assertEquals(expectedNotes.size, actual.size)
            expectedNotes.forEach { expected: Note ->
                assertEquals(expected, actual[expected.id])
            }
        }

        assertThatStorageContains(expectedNotes = emptyArray())

        val note1 = NoteUtils.randomNote()
        writeTransaction { storage.putNote(note1) }
        assertThatStorageContains(note1)

        val note2 = NoteUtils.randomNote()
        writeTransaction { storage.putNote(note2) }
        assertThatStorageContains(note1, note2)

        val note2Updated = NoteUtils.updateNote(note2)
        writeTransaction { storage.putNote(note2Updated) }
        assertThatStorageContains(note1, note2Updated)

        writeTransaction {
            storage.markNoteAsDeleted(note2Updated.id, syncVersion = 2)
        }
        val note2Deleted = note2Updated.copy(deleted = true, syncVersion = 2)
        assertThatStorageContains(note1, note2Deleted)

        val note3 = NoteUtils.randomNote(syncVersion = 3)
        writeTransaction { storage.putNote(note3) }
        assertThatStorageContains(note1, note2Deleted, note3)
    }

    /** Covers setCurrentVersion(), getCurrentVersion() */
    @Test
    fun test2(): Unit = runBlocking {
        assertEquals(0,
            readTransaction { storage.getCurrentVersion() }
        )

        writeTransaction { storage.setCurrentVersion(1) }
        assertEquals(1,
            readTransaction { storage.getCurrentVersion() }
        )

        writeTransaction { storage.setCurrentVersion(2) }
        assertEquals(2,
            readTransaction { storage.getCurrentVersion() }
        )
    }

    /** Covers getNotDeletedNotes() */
    @Test
    fun test3(): Unit = runBlocking {

        suspend fun checkDeletedNotes() {
            readTransaction {
                // we assume that getNotes() works correctly because it is tested
                val expected: Set<Note> = storage.getNotes()
                    .filter { !it.deleted }
                    .toSet()
                val actual: Set<Note> = storage.getNotDeletedNotes().toSet()
                assertSetEquals(expected, actual)
            }
        }

        val note1 = NoteUtils.randomNote()
        writeTransaction { storage.putNote(note1) }
        checkDeletedNotes()

        val note1Deleted = note1.copy(deleted = true, syncVersion = 1)
        writeTransaction { storage.markNoteAsDeleted(note1Deleted.id, syncVersion = 1) }
        checkDeletedNotes()

        val note2 = NoteUtils.randomNote()
        writeTransaction { storage.putNote(note2) }
        checkDeletedNotes()
    }

    /** Covers getNotesModifiedAfterVersion() */
    @Test
    fun test4(): Unit = runBlocking {

        suspend fun checkNotesAfterVersion(fromVersionExclusive: Long, vararg expectedNotes: Note) {
            readTransaction {
                val actualSet: Set<Note> = storage.getNotesModifiedAfterVersion(fromVersionExclusive).toHashSet()
                assertEquals(expectedNotes.size, actualSet.size)
                expectedNotes.forEach { expected: Note ->
                    assert(actualSet.contains(expected))
                }
            }
        }

        checkNotesAfterVersion(-1, expectedNotes = emptyArray())

        val note1 = NoteUtils.randomNote(syncVersion = 1)
        writeTransaction { storage.putNote(note1) }
        checkNotesAfterVersion(-1, expectedNotes = arrayOf(note1))
        checkNotesAfterVersion(0, expectedNotes = arrayOf(note1))
        checkNotesAfterVersion(1, expectedNotes = emptyArray())

        val note2 = NoteUtils.randomNote(syncVersion = 2)
        writeTransaction { storage.putNote(note2) }
        checkNotesAfterVersion(-1, expectedNotes = arrayOf(note1, note2))
        checkNotesAfterVersion(0, expectedNotes = arrayOf(note1, note2))
        checkNotesAfterVersion(1, expectedNotes = arrayOf(note2))
        checkNotesAfterVersion(2, expectedNotes = emptyArray())

        val note1Deleted = note1.copy(deleted = true, syncVersion = 3)
        writeTransaction { storage.putNote(note1Deleted) }
        checkNotesAfterVersion(-1, expectedNotes = arrayOf(note1Deleted, note2))
        checkNotesAfterVersion(0, expectedNotes = arrayOf(note1Deleted, note2))
        checkNotesAfterVersion(1, expectedNotes = arrayOf(note1Deleted, note2))
        checkNotesAfterVersion(2, expectedNotes = arrayOf(note1Deleted))
        checkNotesAfterVersion(3, expectedNotes = emptyArray())
    }
}

private fun <T : Any?> assertSetEquals(expected: Set<T>, actual: Set<T>) {
    assertEquals(expected.size, actual.size)
    assert(expected.containsAll(actual))
}

