package iloginovas.notetoforget.server.model

data class Credentials(
    val username: String,
    val password: String
)