package iloginovas.notetoforget.server.model

data class Note(

    /**
     * Time ordered id. Values ordered by creation date.
     * See [iloginovas.notetoforget.common.NoteId]
     */
    val id: Long,

    val title: String,

    val text: String,

    /**
     * Stored as seconds since epoch
     */
    val lastUpdate: Long,

    val deleted: Boolean,

    /**
     * The version of note's storage [iloginovas.notetoforget.server.data.NoteStorage]
     * when this [Note] was changed for the last time.
     */
    val syncVersion: Long
)