package iloginovas.notetoforget.server.model

data class NewUser(

    /** @see iloginovas.notetoforget.common.validation.UsernameValidator**/
    val name: String,

    /** @see iloginovas.notetoforget.common.validation.PasswordValidator **/
    val password: String
)