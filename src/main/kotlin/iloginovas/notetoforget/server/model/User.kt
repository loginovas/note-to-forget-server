package iloginovas.notetoforget.server.model

data class User(
    val id: Int,

    /** @see iloginovas.notetoforget.common.validation.UsernameValidator**/
    val name: String,

    /** @see iloginovas.notetoforget.common.validation.PasswordValidator **/
    val password: String
)

