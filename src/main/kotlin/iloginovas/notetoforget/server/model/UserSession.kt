package iloginovas.notetoforget.server.model

data class UserSession(
    val sessionId: String,
    val userId: Int
)