package iloginovas.notetoforget.server

import iloginovas.notetoforget.server.routes.loginRoutes
import iloginovas.notetoforget.server.routes.noteRoutes
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import org.slf4j.event.Level
import java.text.DateFormat

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

data class Session(val sessionId: String)

fun Application.module(testing: Boolean = false) {
    DependencyInjection.init(this)

    install(CallLogging) { level = Level.INFO }
    install(DefaultHeaders)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(Sessions) {
        header<Session>("SESSION") {
            serializer = object : SessionSerializer<Session> {

                override fun deserialize(text: String): Session {
                    return Session(text)
                }

                override fun serialize(session: Session): String {
                    return session.sessionId
                }
            }
        }
    }

    routing {
        loginRoutes()
        noteRoutes()

        get("/") {
            call.respondText("Hello, world!")
        }
    }
}