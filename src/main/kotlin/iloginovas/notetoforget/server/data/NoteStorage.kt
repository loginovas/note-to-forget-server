package iloginovas.notetoforget.server.data

import iloginovas.notetoforget.server.model.Note

/**
 * One instance of [NoteStorage] stores notes for one user.
 */
interface NoteStorage : TransactionalStorage {

    fun getNotes(): Sequence<Note>

    /** Returns all the notes that is not deleted (their [Note.deleted] is false) */
    fun getNotDeletedNotes(): Sequence<Note>

    fun getNotesModifiedAfterVersion(fromVersionExclusive: Long): Sequence<Note>

    /** Update or insert */
    fun putNote(note: Note)

    /**
     * Mark note with [noteId] as deleted (@see [Note.deleted]).
     * Do nothing if note with [noteId] does not exist or the note is already removed ([Note.deleted] is true)
     */
    fun markNoteAsDeleted(noteId: Long, syncVersion: Long)

    /** Min value is 0 */
    fun getCurrentVersion(): Long

    fun setCurrentVersion(version: Long)
}