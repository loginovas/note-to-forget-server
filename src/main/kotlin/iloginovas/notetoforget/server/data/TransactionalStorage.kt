package iloginovas.notetoforget.server.data

/**
 * All storage's methods must be called from within a transaction, otherwise an error occurs.
 * You can do it like this:
 * ```
 * val transactionManager: TransactionManager
 * val storage: UserStorage
 * transactionManager.readTransaction {
 *     storage.findByName("some_name")
 * }
 * ```
 */
interface TransactionalStorage