package iloginovas.notetoforget.server.data.impl

import iloginovas.notetoforget.server.data.TransactionManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.concurrent.thread

private const val writeThreadName = "db-writeThread"

class TransactionManagerImpl(private val database: Database) : TransactionManager {
    private val log = LoggerFactory.getLogger(TransactionManagerImpl::class.java)
    private val writeDispatcher: ExecutorCoroutineDispatcher

    init {
        writeDispatcher = newSingleThreadExecutorWithName(writeThreadName)
            .asCoroutineDispatcher()
            .also { closeOnJvmShutdown(it) }
    }

    override suspend fun <T> readTransaction(readFun: () -> T): T =
        withContext(Dispatchers.IO) {
            transaction(database) { readFun() }
        }

    override suspend fun <T> writeTransaction(writeFun: () -> T): T =
        withContext(writeDispatcher) {
            transaction(database) { writeFun() }
        }

    private fun closeOnJvmShutdown(dispatcher: ExecutorCoroutineDispatcher) {
        val name = "shutdownHook"
        Runtime.getRuntime().addShutdownHook(
            thread(start = false, name = name) {
                log.info("$name: shutdown start")
                dispatcher.close()
            })
    }
}

private fun newSingleThreadExecutorWithName(name: String): ExecutorService =
    Executors.newSingleThreadExecutor { Thread(it, name) }