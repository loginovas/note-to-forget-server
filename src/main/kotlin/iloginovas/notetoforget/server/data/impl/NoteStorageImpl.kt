package iloginovas.notetoforget.server.data.impl

import iloginovas.notetoforget.server.data.NoteStorage
import iloginovas.notetoforget.server.data.impl.db.NoteTable
import iloginovas.notetoforget.server.data.impl.db.UserNoteStorageVersionTable
import iloginovas.notetoforget.server.model.Note
import org.jetbrains.exposed.sql.*
import kotlin.sequences.Sequence

object NoteStorageProvider : (Int) -> NoteStorage {
    override fun invoke(userId: Int): NoteStorage = NoteStorageImpl(userId)
}

class NoteStorageImpl(private val userId: Int) : NoteStorage {

    private val noteTable = NoteTable(userId)

    override fun getNotes(): Sequence<Note> {
        return noteTable.selectAll().asSequence().map { noteTable.toModel(it) }
    }

    override fun getNotDeletedNotes(): Sequence<Note> =
        noteTable
            .select { noteTable.deleted eq false }
            .asSequence()
            .map { noteTable.toModel(it) }

    override fun getCurrentVersion(): Long =
        UserNoteStorageVersionTable
            .select { UserNoteStorageVersionTable.userId eq userId }
            .singleOrNull()?.get(UserNoteStorageVersionTable.version) ?: 0L

    override fun setCurrentVersion(version: Long) {
        val exists = UserNoteStorageVersionTable
            .select { UserNoteStorageVersionTable.userId eq userId }
            .count() > 0

        if (exists) {
            UserNoteStorageVersionTable.update({
                UserNoteStorageVersionTable.userId eq userId
            }) {
                it[UserNoteStorageVersionTable.version] = version
            }
        } else {
            UserNoteStorageVersionTable.insert {
                it[UserNoteStorageVersionTable.userId] = this@NoteStorageImpl.userId
                it[UserNoteStorageVersionTable.version] = version
            }
        }
    }

    override fun getNotesModifiedAfterVersion(fromVersionExclusive: Long): Sequence<Note> =
        noteTable
            .select { noteTable.syncVersion greater fromVersionExclusive }
            .asSequence()
            .map { noteTable.toModel(it) }

    override fun markNoteAsDeleted(noteId: Long, syncVersion: Long) {
        noteTable.update({
            (noteTable.id eq noteId) and (noteTable.deleted eq false)
        }) {
            it[noteTable.deleted] = true
            it[noteTable.syncVersion] = syncVersion
        }
    }

    override fun putNote(note: Note) {
        val exitsInDb: Boolean = noteTable.select { noteTable.id eq note.id }.count() > 0

        if (exitsInDb) noteTable.update({ noteTable.id eq note.id }) {
            it[title] = note.title
            it[text] = note.text
            it[lastUpdate] = note.lastUpdate
            it[deleted] = note.deleted
            it[syncVersion] = note.syncVersion
        }
        else noteTable.insert {
            it[id] = note.id
            it[title] = note.title
            it[text] = note.text
            it[lastUpdate] = note.lastUpdate
            it[deleted] = note.deleted
            it[syncVersion] = note.syncVersion
        }
    }
}

fun NoteTable.toModel(row: ResultRow) = Note(
    row[id], row[title], row[text], row[lastUpdate], row[deleted], row[syncVersion]
)