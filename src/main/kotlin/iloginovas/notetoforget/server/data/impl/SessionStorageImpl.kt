package iloginovas.notetoforget.server.data.impl

import iloginovas.notetoforget.server.data.SessionStorage
import iloginovas.notetoforget.server.data.impl.db.SessionEntity
import iloginovas.notetoforget.server.model.UserSession
import java.util.*

class SessionStorageImpl : SessionStorage {

    override fun createSession(userId: Int): UserSession {
        val sessionEntity = SessionEntity.new(UUID.randomUUID()) {
            this.userId = userId
        }
        return sessionEntity.toModel()
    }

    override fun deleteSession(sessionId: String) {
        SessionEntity.findById(UUID.fromString(sessionId))?.delete()
    }

    override fun findById(sessionId: String): UserSession? =
        SessionEntity.findById(UUID.fromString(sessionId))
            ?.toModel()

}

private fun SessionEntity.toModel(): UserSession =
    UserSession(id.value.toString(), userId)