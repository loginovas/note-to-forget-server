package iloginovas.notetoforget.server.data.impl.db

import org.jetbrains.exposed.sql.Table

class NoteTable(private val userId: Int) : Table("note_$userId") {

    /**
     * Time ordered id. Values ordered by creation date.
     * See [iloginovas.notetoforget.common.NoteId]
     */
    val id = long("id")

    val title = text("title")

    val text = text("text")

    /** Stored as seconds since epoch */
    val lastUpdate = long("last_update")

    val deleted = bool("deleted").default(false)

    val syncVersion = long("sync_version").index()

    override val primaryKey = PrimaryKey(id)
}