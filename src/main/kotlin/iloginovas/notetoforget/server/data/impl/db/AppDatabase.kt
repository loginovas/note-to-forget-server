package iloginovas.notetoforget.server.data.impl.db

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.sqlite.SQLiteConfig
import org.sqlite.SQLiteDataSource
import java.sql.Connection

const val dbFilename = "notes.db"

object AppDatabase {

    val tables = arrayOf(UserTable, SessionTable, UserNoteStorageVersionTable)

    fun create(
        inMemory: Boolean = false,
        createSchema: Boolean = true
    ): Database {
        val db = registerDatabase(inMemory)

        if (createSchema)
            createSchema(db)

        return db
    }

    private fun registerDatabase(inMemory: Boolean): Database {
        val hikariConfig = HikariConfig().apply {
            maximumPoolSize = 10
            dataSource = SQLiteDataSource().apply {
                url = if (inMemory) "jdbc:sqlite::memory:" else "jdbc:sqlite:$dbFilename"
                setJournalMode(SQLiteConfig.JournalMode.WAL.value)
            }
        }
        val db = Database.connect(HikariDataSource(hikariConfig))
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
        return db
    }

    private fun createSchema(db: Database) {
        transaction(db) {
            SchemaUtils.create(*tables)
        }
    }
}