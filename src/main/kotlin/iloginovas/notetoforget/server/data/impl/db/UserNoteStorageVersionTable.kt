package iloginovas.notetoforget.server.data.impl.db

import org.jetbrains.exposed.sql.Table

object UserNoteStorageVersionTable : Table("user_note_storage_version") {
    val userId = integer("user_id")
    val version = long("version")

    override val primaryKey = PrimaryKey(userId)
}