package iloginovas.notetoforget.server.data.impl

import iloginovas.notetoforget.server.data.UserStorage
import iloginovas.notetoforget.server.data.impl.db.UserEntity
import iloginovas.notetoforget.server.data.impl.db.UserTable
import iloginovas.notetoforget.server.model.NewUser
import iloginovas.notetoforget.server.model.User

class UserStorageImpl : UserStorage {
    private val chunkSize = 64

    override fun findById(id: Int): User? =
        UserEntity.findById(id)?.toUser()

    override fun findByIds(ids: List<Int>): List<User> {
        if (ids.size <= chunkSize)
            return UserEntity.find { UserTable.id inList ids }
                .map(UserEntity::toUser)

        return ids.asSequence()
            .chunked(chunkSize)
            .flatMapTo(ArrayList(ids.size)) {
                UserEntity.find { UserTable.id inList ids }
                    .map(UserEntity::toUser)
            }
    }

    override fun findByName(username: String): User? {
        val name = UserEntity.normalizeUsername(username)
        return UserEntity.find { UserTable.name eq name }
            .limit(1)
            .firstOrNull()?.toUser()
    }

    override fun createUser(newUser: NewUser): User {
        val userEntity = UserEntity.new {
            name = UserEntity.normalizeUsername(newUser.name)
            password = UserEntity.normalizePassword(newUser.password)
        }
        return userEntity.toUser()
    }
}

private fun UserEntity.toUser(): User =
    User(id.value, name, password)