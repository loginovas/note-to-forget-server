package iloginovas.notetoforget.server.data

import iloginovas.notetoforget.server.model.NewUser
import iloginovas.notetoforget.server.model.User

interface UserStorage : TransactionalStorage {
    fun findById(id: Int): User?
    fun findByIds(ids: List<Int>): List<User>
    fun findByName(username: String): User?

    fun createUser(newUser: NewUser): User
}

