package iloginovas.notetoforget.server.data

import iloginovas.notetoforget.server.model.UserSession

interface SessionStorage : TransactionalStorage {

    fun createSession(userId: Int): UserSession

    fun deleteSession(sessionId: String)

    fun findById(sessionId: String): UserSession?
}

