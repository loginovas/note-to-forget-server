package iloginovas.notetoforget.server

import iloginovas.notetoforget.server.data.SessionStorage
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.UserStorage
import iloginovas.notetoforget.server.data.impl.NoteStorageProvider
import iloginovas.notetoforget.server.data.impl.SessionStorageImpl
import iloginovas.notetoforget.server.data.impl.TransactionManagerImpl
import iloginovas.notetoforget.server.data.impl.UserStorageImpl
import iloginovas.notetoforget.server.data.impl.db.AppDatabase
import iloginovas.notetoforget.server.service.*
import io.ktor.application.*
import io.ktor.util.*
import org.jetbrains.exposed.sql.Database

fun MutableMap<Class<*>, Any>.dependencies() {
    /* Data */
    val database: Database = AppDatabase.create(inMemory = false, createSchema = true)
    singleton<TransactionManager>(TransactionManagerImpl(database))
    singleton<UserStorage>(UserStorageImpl())
    singleton<SessionStorage>(SessionStorageImpl())

    /* Services */
    singleton<UserService>(UserServiceImpl(getInstance(), getInstance()))
    singleton<SessionManager>(SessionManagerImpl(getInstance(), getInstance(), getInstance()))
    singleton(NoteService(getInstance(), NoteStorageProvider))
}

class DependencyInjection {
    private val classToInstanceMap: Map<Class<*>, Any>

    init {
        val map = HashMap<Class<*>, Any>()
        map.dependencies()
        classToInstanceMap = map
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getInstance(cls: Class<T>): T =
        classToInstanceMap[cls] as? T ?: error("Instance 'cls' not found")

    companion object {
        private val dependencyInjectionAttrKey =
            AttributeKey<DependencyInjection>("DependencyInjection")

        fun init(app: Application, di: DependencyInjection = DependencyInjection()) {
            app.attributes.put(dependencyInjectionAttrKey, di)
        }

        inline fun <reified T : Any> Application.getInstance(): T {
            return this.getInstance(T::class.java)
        }

        fun <T : Any> Application.getInstance(cls: Class<T>): T =
            getDI().getInstance(cls)


        private fun Application.getDI(): DependencyInjection =
            attributes[dependencyInjectionAttrKey]
    }
}

private inline fun <reified T : Any> MutableMap<Class<*>, Any>.singleton(instance: T) {
    put(T::class.java, instance)
}

private inline fun <reified T : Any> MutableMap<Class<*>, Any>.singleton(
    instanceProvider: () -> T
) {
    put(T::class.java, instanceProvider())
}

private inline fun <reified T : Any> Map<Class<*>, Any>.getInstance(): T {
    return get(T::class.java) as T
}

