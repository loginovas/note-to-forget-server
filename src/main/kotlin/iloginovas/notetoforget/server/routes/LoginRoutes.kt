package iloginovas.notetoforget.server.routes

import com.github.michaelbull.result.getOrElse
import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.common.Api.LoginResponse
import iloginovas.notetoforget.common.login.LoginErrorCause
import iloginovas.notetoforget.common.login.WrongCredentials
import iloginovas.notetoforget.server.DependencyInjection.Companion.getInstance
import iloginovas.notetoforget.server.Session
import iloginovas.notetoforget.server.model.Credentials
import iloginovas.notetoforget.server.model.User
import iloginovas.notetoforget.server.model.UserSession
import iloginovas.notetoforget.server.service.SessionManager
import iloginovas.notetoforget.server.service.UserService
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*

fun Routing.loginRoutes() {
    val userService = application.getInstance<UserService>()
    val sessionManager = application.getInstance<SessionManager>()


    post("/user/registration") {
        val credentials: Credentials = call.receive<Api.UserCredentials>().toModel()

        val user: User = userService.registerUser(credentials)
            .getOrElse { error: LoginErrorCause ->
                val response = LoginResponse.failure(error)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        val session: UserSession = sessionManager.newSession(user) ?: kotlin.run {
            val error: LoginErrorCause = WrongCredentials
            val response = LoginResponse.failure(error)
            call.respond(HttpStatusCode.Unauthorized, response)
            return@post
        }

        call.sessions.set(Session(session.sessionId))
        val response = LoginResponse.success(
            sessionId = session.sessionId,
            user = Api.User(user.id, user.name)
        )
        call.respond(response)
    }


    post("/user/logout") {
        call.sessions.get<Session>()?.let {
            sessionManager.dropSession(it.sessionId)
            call.sessions.clear<Session>()
        }
        call.respond(HttpStatusCode.OK)
    }


    post("/user/login") {
        val credentials: Credentials = call.receive<Api.UserCredentials>().toModel()

        val user: User = userService.authenticate(credentials)
            .getOrElse { error: LoginErrorCause ->
                val response = LoginResponse.failure(error)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        val session: UserSession = sessionManager.newSession(user) ?: kotlin.run {
            val error: LoginErrorCause = WrongCredentials
            val response = LoginResponse.failure(error)
            call.respond(HttpStatusCode.Unauthorized, response)
            return@post
        }

        call.sessions.set(Session(session.sessionId))
        val response = LoginResponse.success(
            sessionId = session.sessionId,
            user = Api.User(user.id, user.name)
        )
        call.respond(response)
    }
}

private fun Api.UserCredentials.toModel() = Credentials(username, password)