package iloginovas.notetoforget.server.routes

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.server.DependencyInjection.Companion.getInstance
import iloginovas.notetoforget.server.Session
import iloginovas.notetoforget.server.model.UserSession
import iloginovas.notetoforget.server.service.NoteService
import iloginovas.notetoforget.server.service.SessionManager
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*

fun Routing.noteRoutes() {

    val noteService: NoteService = application.getInstance()
    val sessionManager: SessionManager = application.getInstance()

    post("sync") {
        val userSession: UserSession = call.sessions.get<Session>()
            ?.let { sessionManager.getSessionById(it.sessionId) }
            ?: kotlin.run {
                call.respond(HttpStatusCode.Unauthorized)
                return@post
            }

        val syncRequest = call.receive<Api.NoteChanges>()
        val syncResponse: Api.NoteChanges = noteService.sync(userSession, syncRequest)
        call.respond(HttpStatusCode.OK, syncResponse)
    }
}