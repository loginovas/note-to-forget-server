package iloginovas.notetoforget.server.service

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.server.data.NoteStorage
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.TransactionSupport
import iloginovas.notetoforget.server.model.Note
import iloginovas.notetoforget.server.model.UserSession

class NoteService(
    override val transactionManager: TransactionManager,
    private val noteStorageProvider: (userId: Int) -> NoteStorage
) : TransactionSupport {

    suspend fun sync(
        userSession: UserSession,
        syncRequest: Api.NoteChanges
    ): Api.NoteChanges {

        val noteStorage = noteStorageProvider(userSession.userId)

        val (changesForClient: HashMap<Long, Note>, currentVersion: Long) =
            writeTransaction {

                val currentVersion: Long = noteStorage.getCurrentVersion()
                if (syncRequest.version > currentVersion) throw IllegalArgumentException()

                val serverClientDiff: HashMap<Long, Note> =
                    noteStorage.getNotesModifiedAfterVersion(syncRequest.version)
                    .associateByTo(HashMap()) { it.id }

                if (syncRequest.notesToBePut.isEmpty() && syncRequest.notesToBeRemoved.isEmpty()) {
                    return@writeTransaction (serverClientDiff to currentVersion)
                }

                val nextServerVer = currentVersion + 1

                syncRequest.notesToBeRemoved.forEach { noteId ->
                    serverClientDiff.remove(noteId) //if noteId exists
                    noteStorage.markNoteAsDeleted(noteId, nextServerVer)
                }

                syncRequest.notesToBePut.forEach { noteApi: Api.Note ->
                    val serverNote = serverClientDiff[noteApi.id]
                    val clientNote = Note(noteApi.id, noteApi.title, noteApi.text, noteApi.lastUpdate, false, nextServerVer)
                    if (serverNote == null) {
                        noteStorage.putNote(clientNote)
                    } else if (clientNote.lastUpdate > serverNote.lastUpdate) {
                        noteStorage.putNote(clientNote)
                        serverClientDiff.remove(noteApi.id)
                    }
                }

                noteStorage.setCurrentVersion(nextServerVer)
                serverClientDiff to nextServerVer
            }

        return Api.NoteChanges(
            currentVersion,
            changesForClient.asSequence()
                .filter { it.value.deleted }
                .map { it.key }
                .toList(),
            changesForClient.asSequence()
                .map { it.value }
                .filter { !it.deleted }
                .map { Api.Note(it.id, it.title, it.text, it.lastUpdate) }
                .toList()
        )
    }
}