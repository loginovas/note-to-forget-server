package iloginovas.notetoforget.server.service

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import iloginovas.notetoforget.common.login.InvalidFormat
import iloginovas.notetoforget.common.login.LoginErrorCause
import iloginovas.notetoforget.common.login.UsernameAlreadyExists
import iloginovas.notetoforget.common.login.WrongCredentials
import iloginovas.notetoforget.common.validation.PasswordErrorCause
import iloginovas.notetoforget.common.validation.PasswordValidator
import iloginovas.notetoforget.common.validation.UsernameErrorCause
import iloginovas.notetoforget.common.validation.UsernameValidator
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.TransactionSupport
import iloginovas.notetoforget.server.data.UserStorage
import iloginovas.notetoforget.server.data.impl.db.NoteTable
import iloginovas.notetoforget.server.data.impl.db.UserNoteStorageVersionTable
import iloginovas.notetoforget.server.model.Credentials
import iloginovas.notetoforget.server.model.NewUser
import iloginovas.notetoforget.server.model.User
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert

class UserServiceImpl(
    private val userStorage: UserStorage,
    override val transactionManager: TransactionManager
) : UserService, TransactionSupport {

    override suspend fun authenticate(credentials: Credentials): Result<User, LoginErrorCause> {
        val (username, password) = credentials
        val user = findUserByName(username) ?: return Err(WrongCredentials)
        return when (user.password == password.trim()) {
            true -> Ok(user)
            else -> Err(WrongCredentials)
        }
    }

    override suspend fun registerUser(credentials: Credentials): Result<User, LoginErrorCause> {
        val (username, password) = credentials

        UsernameValidator.validate(username)
            ?.let { error: UsernameErrorCause ->
                return Err(InvalidFormat(usernameError = error))
            }
        PasswordValidator.validate(password)
            ?.let { error: PasswordErrorCause ->
                return Err(InvalidFormat(passwordError = error))
            }

        findUserByName(username)?.let {
            return Err(UsernameAlreadyExists)
        }

        return writeTransaction {
            userStorage.findByName(username)?.let {
                return@writeTransaction Err(UsernameAlreadyExists)
            }
            val newUser = userStorage.createUser(NewUser(username, password))
            SchemaUtils.create(NoteTable(newUser.id))
            UserNoteStorageVersionTable.insert {
                it[userId] = newUser.id
                it[version] = 0
            }
            Ok(newUser)
        }
    }

    override suspend fun findUserById(id: Int): User? =
        readTransaction {
            userStorage.findById(id)
        }

    override suspend fun findUserByName(username: String): User? =
        readTransaction {
            userStorage.findByName(username)
        }
}