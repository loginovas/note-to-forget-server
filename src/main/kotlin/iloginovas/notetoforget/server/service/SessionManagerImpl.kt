package iloginovas.notetoforget.server.service

import iloginovas.notetoforget.server.data.SessionStorage
import iloginovas.notetoforget.server.data.TransactionManager
import iloginovas.notetoforget.server.data.TransactionSupport
import iloginovas.notetoforget.server.data.UserStorage
import iloginovas.notetoforget.server.model.User
import iloginovas.notetoforget.server.model.UserSession

class SessionManagerImpl(
    override val transactionManager: TransactionManager,
    private val userStorage: UserStorage,
    private val sessionStorage: SessionStorage
) : SessionManager, TransactionSupport {

    override suspend fun newSession(user: User): UserSession? =
        writeTransaction {
            if (userStorage.findById(user.id) == null)
                null
            else
                sessionStorage.createSession(user.id)
        }


    override suspend fun getSessionById(sessionId: String): UserSession? =
        readTransaction {
            sessionStorage.findById(sessionId)
        }

    override suspend fun dropSession(sessionId: String) {
        writeTransaction {
            sessionStorage.deleteSession(sessionId)
        }
    }
}