package iloginovas.notetoforget.server.service

import com.github.michaelbull.result.Result
import iloginovas.notetoforget.common.login.LoginErrorCause
import iloginovas.notetoforget.server.model.Credentials
import iloginovas.notetoforget.server.model.User

interface UserService {

    suspend fun authenticate(credentials: Credentials): Result<User, LoginErrorCause>

    suspend fun registerUser(credentials: Credentials): Result<User, LoginErrorCause>

    suspend fun findUserById(id: Int): User?

    suspend fun findUserByName(username: String): User?
}

