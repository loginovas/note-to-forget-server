package iloginovas.notetoforget.server.service

import iloginovas.notetoforget.server.model.User
import iloginovas.notetoforget.server.model.UserSession

interface SessionManager {

    /**
     * @return new session or null if [user] does not exist
     **/
    suspend fun newSession(user: User): UserSession?

    suspend fun getSessionById(sessionId: String): UserSession?

    /**
     * Does nothing if session with [sessionId] does not exist
     */
    suspend fun dropSession(sessionId: String)
}

